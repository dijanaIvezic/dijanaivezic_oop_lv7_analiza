﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TTT
{
    public partial class Ttt : Form
    {
        Graphics g;
        bool turn = false;
        bool started = false;
        int[,] arr = new int[3, 3];

        int p1_score = 0;
        public int P1_score
        {
            get
            {
                return p1_score;
            }
            set
            {
                p1_score = value;
                lP1.Text = p1_score.ToString();
            }
        }
        int p2_score = 0;
        public int P2_score
        {
            get
            {
                return p2_score;
            }
            set
            {
                p2_score = value;
                lP2.Text = p2_score.ToString();
            }
        }
        public Ttt()
        {
            InitializeComponent();

        }

        private void Ttt_Load(object sender, EventArgs e)
        {
            g = pb.CreateGraphics();
            P1_score = 0;
            P2_score = 0;
            setArrZ();
            
        }

        void setArrZ() {
            //fill arr with 0
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    arr[i, j] = 0;
                }
            }
        }

        private void bS_Click(object sender, EventArgs e)
        {
            started = true;
            p1Name.Enabled = false;
            p2Name.Enabled = false;
            turn = false;
            setArrZ();
            g.Clear(Color.White);
            g.DrawLine(new Pen(Color.Black, 1.0f), new Point(100, 0), new Point(100, 300));
            g.DrawLine(new Pen(Color.Black, 1.0f), new Point(200, 0), new Point(200, 300));
            g.DrawLine(new Pen(Color.Black, 1.0f), new Point(0, 100), new Point(300, 100));
            g.DrawLine(new Pen(Color.Black, 1.0f), new Point(0, 200), new Point(300, 200));
        }

        private void pb_MouseUp(object sender, MouseEventArgs e)
        {
            if (started)
            {
                int i = 0;
                int j = 0;
                if (e.X >= 100 && e.X <= 200)
                {
                    i = 1;
                }
                else if(e.X > 200 && e.X <= 300) {
                    i = 2;
                }
                if (e.Y >= 100 && e.Y <= 200)
                {
                    j = 1;
                }
                else if (e.Y > 200 && e.Y <= 300)
                {
                    j = 2;
                }

                    Point p = new Point(e.X, e.Y);
                    Pen xp = new Pen(Color.Red, 2.0f);
                    Pen op = new Pen(Color.Blue, 2.0f);
                    if (!turn)
                    {
                        X x = new X(p);
                        x.draw(g, xp);
                        turn = !turn;
                        arr[i, j] = 1;
                    }
                    else
                    {
                        O o = new O(p);
                        o.draw(g, op);
                        turn = !turn;
                        arr[i, j] = 2;
                    }
                    if (isDraw() && !checkWin()) {
                        MessageBox.Show("It is a tie.", "Draw");
                    }
                    else if(checkWin()){
                        string msg;
                        if (turn)
                        {
                            msg = p1Name.Text + ", you win!";
                            MessageBox.Show(msg, "Win");
                            P1_score++;
                        }
                        else {
                            msg = p2Name.Text + ", you win!";
                            MessageBox.Show(msg, "Win");
                            P2_score++;
                            swapNames();
                    }
                  }
            }
        }
        class X
        {
            Point c;
            public X(Point center)
            {
                c = center;
            }
            public void draw(Graphics g, Pen p)
            {
                int s = 20;
                int x = c.X;
                int y = c.Y;
                Point f1 = new Point(x - s, y + s);
                Point f2 = new Point(x + s, y - s);
                Point s1 = new Point(x + s, y + s);
                Point s2 = new Point(x - s, y - s);
                g.DrawLine(p, f1, f2);
                g.DrawLine(p, s1, s2);
            }
        }
        class O
        {
            Point c;
            public O(Point center)
            {
                c = center;
            }
            public void draw(Graphics g, Pen p)
            {
                int s = 40;
                Size si = new Size(s, s);
                Rectangle r = new Rectangle(new Point((c.X - (s / 2)), (c.Y - (s / 2))), si);
                g.DrawEllipse(p, r);
            }
        }
        bool checkWin() {
            int h1= 0;
            int h2 = 0;
            int v1 = 0;
            int v2 = 0;
            int d11 = 0;
            int d12 = 0;
            int d21 = 0;
            int d22 = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (arr[i, j] == 1) {
                        h1++;
                    }
                    if (arr[i, j] == 2)
                    {
                        h2++;
                    }
                    if (arr[j, i] == 1) {
                        v1++;
                    }
                    if (arr[j, i] == 2) {
                        v2++;
                    }
                }
                if (h1 == 3 || h2 == 3 || v1 == 3 || v2 == 3)
                {
                    setArrZ();
                    return true;
                }
                else {
                    h1 = 0;
                    h2 = 0;
                    v1 = 0;
                    v2 = 0;
                }
            }
            for (int i = 0; i < 3; i++)
            {
                if (arr[i, i] == 1)
                {
                    d11++;
                }
                if (arr[i, i] == 2)
                {
                    d12++;
                }
                if (arr[2 - i, i] == 1)
                {
                    d21++;
                }
                if (arr[2 - i, i] == 2)
                {
                    d22++;
                }
            }
            if (d11 == 3 || d12 == 3 || d21 == 3 || d22 == 3) {
                return true;
            }
            return false;
        }
        bool isDraw() {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (arr[i, j] == 0) {
                        return false;
                    }
                }
            }
            return true;
        }
        void swapNames() {
            string tmp = p2Name.Text;
            int t = P2_score;
            p2Name.Text = p1Name.Text;
            p1Name.Text = tmp;
            P2_score = P1_score;
            P1_score = t;
        }
    }
}

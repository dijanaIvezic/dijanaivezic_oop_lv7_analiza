﻿namespace TTT
{
    partial class Ttt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lP1 = new System.Windows.Forms.Label();
            this.lP2 = new System.Windows.Forms.Label();
            this.pb = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bS = new System.Windows.Forms.Button();
            this.p1Name = new System.Windows.Forms.TextBox();
            this.p2Name = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.SuspendLayout();
            // 
            // lP1
            // 
            this.lP1.AutoSize = true;
            this.lP1.Location = new System.Drawing.Point(192, 9);
            this.lP1.Name = "lP1";
            this.lP1.Size = new System.Drawing.Size(18, 13);
            this.lP1.TabIndex = 0;
            this.lP1.Text = "s1";
            // 
            // lP2
            // 
            this.lP2.AutoSize = true;
            this.lP2.Location = new System.Drawing.Point(192, 35);
            this.lP2.Name = "lP2";
            this.lP2.Size = new System.Drawing.Size(18, 13);
            this.lP2.TabIndex = 1;
            this.lP2.Text = "s2";
            // 
            // pb
            // 
            this.pb.BackColor = System.Drawing.Color.White;
            this.pb.Location = new System.Drawing.Point(7, 65);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(300, 300);
            this.pb.TabIndex = 2;
            this.pb.TabStop = false;
            this.pb.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Player1: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Player2: ";
            // 
            // bS
            // 
            this.bS.Location = new System.Drawing.Point(232, 25);
            this.bS.Name = "bS";
            this.bS.Size = new System.Drawing.Size(75, 23);
            this.bS.TabIndex = 5;
            this.bS.Text = "Start";
            this.bS.UseVisualStyleBackColor = true;
            this.bS.Click += new System.EventHandler(this.bS_Click);
            // 
            // p1Name
            // 
            this.p1Name.Location = new System.Drawing.Point(57, 6);
            this.p1Name.Name = "p1Name";
            this.p1Name.Size = new System.Drawing.Size(109, 20);
            this.p1Name.TabIndex = 6;
            // 
            // p2Name
            // 
            this.p2Name.Location = new System.Drawing.Point(57, 32);
            this.p2Name.Name = "p2Name";
            this.p2Name.Size = new System.Drawing.Size(109, 20);
            this.p2Name.TabIndex = 7;
            // 
            // Ttt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 371);
            this.Controls.Add(this.p2Name);
            this.Controls.Add(this.p1Name);
            this.Controls.Add(this.bS);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.lP2);
            this.Controls.Add(this.lP1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Ttt";
            this.Text = "TTT";
            this.Load += new System.EventHandler(this.Ttt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lP1;
        private System.Windows.Forms.Label lP2;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bS;
        private System.Windows.Forms.TextBox p1Name;
        private System.Windows.Forms.TextBox p2Name;
    }
}

